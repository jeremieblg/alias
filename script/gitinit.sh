#!/bin/bash

default_branch="main"

echo "git init"
git init
echo "set default branch"
git symbolic-ref HEAD refs/heads/$default_branch
echo "git add"
git add .
echo "git commit"
git commit -m "initial commit"
echo "Push to new repo"
git push --set-upstream git@gitlab.com:jeremieblg/$1.git $default_branch
echo "Add origin"
git remote add origin git@gitlab.com:jeremieblg/$1.git
echo "Fetch"
git fetch origin $default_branch
echo "Setup tracking branch"
git branch -u origin/$default_branch